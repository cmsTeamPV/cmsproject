/**
 * Created by jaburur on 11-03-2016.
 */
var PV_CMS = PV_CMS || {};
PV_CMS.controller = PV_CMS.controller || {};

PV_CMS.controller.baseController = function($scope,model){
    $scope.appVersion= model.getAppVersion();

    $scope.selectedHeaderMenu = 'sub';
    $scope.menuItems=[
        {
            name:"Admin",
            tab:"adm"
        },
        {
            name:"Subscriber",
            tab:"sub"
        },
        {
            name:"Marketing",
            tab:"mrk"
        },
        {
            name:"Content",
            tab:"cnt"
        },
        {
            name:"Setting",
            tab:"set"
        },
        {
            name:"Analytics",
            tab:"anl"
        }
    ];
    $scope.onHeaderMenuClick=function(tab){
        $scope.selectedHeaderMenu=tab;
    };


};

angular.module('pvCMS', [])
    .controller('baseController', ['$scope','Model', PV_CMS.controller.baseController]);