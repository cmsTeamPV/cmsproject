/**
 * Created by jaburur on 11-03-2016.
 */


var express = require('express'),
    path = require('path'),
    app = express(),
    fs= require('fs');

//const
var dir = __dirname;

//app.use(express.static('css'));
app.use("/css", express.static(dir + '/css'));
app.use("/images",express.static(dir + '/images'));
app.use("/lib", express.static(dir + '/lib'));
app.use("/src",express.static(dir + '/src'));
app.use("/server",express.static(dir + '/server'));

app.get('/index', function (req, res) {
    var partials="";
    fs.readdir(dir+"\\src\\partial", function(err, files) {
        if (err) {
            res.send(err);
        }
        files.forEach(function(f) {
            fs.readFile(dir+"\\src\\partial\\"+f, function (err, data) {
                if (err) {
                    console.log(err);
                }
                partials=partials+data.toString();
            });
        });

        fs.readFile(dir+"\\index.html" , function (err, data) {
            if (err) {
                res.send(err);
            }
            var strHtml = data.toString();
            strHtml= strHtml.replace(/<!-- partials -->/g,partials);
            res.send(strHtml);
        });
    });

});

//app.get('/process_get', function (req, res) {
//
//    // Prepare output in JSON format
//    response = {
//        first_name:req.query.first_name,
//        last_name:req.query.last_name
//    };
//    console.log(response);
//    res.end(JSON.stringify(response));
//})

var server = app.listen(2525, function () {

    var host = server.address().address;
    var port = server.address().port;

    console.log("Example app listening at http://%s:%s", host, port)

})